package br.com.querobacon.gitpopular.rest.model;

import java.util.List;

/**
 * Created by arthu on 01/03/2018.
 */

public class Repos {
    private List<Items> items;

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }
}
