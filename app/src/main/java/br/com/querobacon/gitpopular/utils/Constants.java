package br.com.querobacon.gitpopular.utils;

/**
 * Created by arthu on 01/03/2018.
 */

public class Constants {

    public static final String END_POINT = "https://api.github.com/";
    public static final String OWNER_NAME = "owner_name";
    public static final String REPO_NAME = "repo_name";
}
