package br.com.querobacon.gitpopular.rest;

public interface ClientApi<T> {
    T getApi();
    T getApi(String url);
}
