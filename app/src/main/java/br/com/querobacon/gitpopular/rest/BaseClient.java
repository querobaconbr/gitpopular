package br.com.querobacon.gitpopular.rest;

public class BaseClient extends BaseRestClient implements ClientApi<BaseAPI>{

    @Override
    public BaseAPI getApi () {
        setupRestClient();
        return retrofit.create(BaseAPI.class);
    }

    @Override
    public BaseAPI getApi (String url) {
        setupRestClient(url);
        return retrofit.create(BaseAPI.class);
    }
}
