package br.com.querobacon.gitpopular.rest;

import java.util.List;

import br.com.querobacon.gitpopular.rest.model.Pull;
import br.com.querobacon.gitpopular.rest.model.Repos;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BaseAPI {


    @GET("search/repositories?q=language:Java&sort=stars&order=desc")
    Call<Repos> getRepos(@Query("page") int page);

    @GET("/repos/{owner}/{repo}/pulls")
    Call<List<Pull>> getPulls(@Path("owner") String owner, @Path("repo") String repo);

}