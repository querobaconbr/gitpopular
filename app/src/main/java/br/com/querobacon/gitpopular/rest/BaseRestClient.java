package br.com.querobacon.gitpopular.rest;

import br.com.querobacon.gitpopular.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


class BaseRestClient {

    Retrofit retrofit;
    private OkHttpClient.Builder mClient;

    BaseRestClient() {
        mClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        mClient.addInterceptor(loggingInterceptor);

    }

    void setupRestClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.END_POINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mClient.build())
                .build();
    }


    void setupRestClient(String url) {
        retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(mClient.build())
                .build();
    }
}
