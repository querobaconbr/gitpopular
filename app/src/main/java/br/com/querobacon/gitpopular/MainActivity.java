package br.com.querobacon.gitpopular;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import br.com.querobacon.gitpopular.adapters.RepoAdapter;
import br.com.querobacon.gitpopular.rest.BaseClient;
import br.com.querobacon.gitpopular.rest.model.Repos;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRepoList;
    private BaseClient mClient;
    private RepoAdapter mRepoAdapter;
    private LinearLayoutManager mLayoutManager;
    private boolean mIsLoading = false;
    private boolean mIsLastPage = false;
    private int mPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mClient = new BaseClient();
        mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRepoList = (RecyclerView) findViewById(R.id.repo_list);
        mRepoList.setLayoutManager(mLayoutManager);
        mRepoAdapter = new RepoAdapter(this);
        mRepoList.setAdapter(mRepoAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL);
        mRepoList.addItemDecoration(dividerItemDecoration);

        mRepoList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();

                if (!mIsLoading && !mIsLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 30) {
                        mPage++;
                        loadRepos(mPage);
                    }
                }
            }
        });

        loadRepos(mPage);
    }

    public void loadRepos (int page) {
        final Call<Repos> repos = mClient.getApi().getRepos(page);
        mIsLoading = true;
        repos.enqueue(new Callback<Repos>() {
            @Override
            public void onResponse(Call<Repos> call, Response<Repos> response) {
                if (response.isSuccessful()) {
                    if (!response.body().getItems().isEmpty()) {
                        mRepoAdapter.addItems(response.body().getItems());
                    } else {
                        mIsLastPage = true;
                    }
                }
                mIsLoading = false;
            }

            @Override
            public void onFailure(Call<Repos> call, Throwable t) {
                Toast.makeText(MainActivity.this, getString(R.string.error),Toast.LENGTH_LONG).show();
                mIsLoading = false;
            }
        });
    }
}
