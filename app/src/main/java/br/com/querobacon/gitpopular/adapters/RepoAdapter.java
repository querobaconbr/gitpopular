package br.com.querobacon.gitpopular.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.querobacon.gitpopular.PullRequestActivity;
import br.com.querobacon.gitpopular.R;
import br.com.querobacon.gitpopular.rest.model.Items;
import br.com.querobacon.gitpopular.utils.Constants;

/**
 * Created by arthu on 01/03/2018.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.ViewHolder> {

    private List<Items> mRepoList;
    private Context mContext;

    public RepoAdapter(Context context) {
        mRepoList = new ArrayList<>();
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_repo_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Items repo = mRepoList.get(position);
        holder.repoName.setText(repo.getName());
        Picasso.with(mContext).load(repo.getOwner().getAvatar_url()).into(holder.authorImage);
        holder.repoName.setText(repo.getName());
        holder.authorName.setText(repo.getOwner().getLogin());
        holder.starsNumber.setText(mContext.getResources().getQuantityString(R.plurals.stars_number, repo.getStargazers_count(), String.valueOf(repo.getStargazers_count())));
        holder.forksNumber.setText(mContext.getResources().getQuantityString(R.plurals.fork_number, repo.getForks_count(), String.valueOf(repo.getForks_count())));
        holder.repoDescription.setText(mContext.getResources().getString(R.string.description, repo.getDescription()));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PullRequestActivity.class);
                intent.putExtra(Constants.OWNER_NAME, repo.getOwner().getLogin());
                intent.putExtra(Constants.REPO_NAME, repo.getName());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mRepoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView repoName;
        ImageView authorImage;
        TextView authorName;
        TextView starsNumber;
        TextView forksNumber;
        TextView repoDescription;

        public ViewHolder(View v) {
            super(v);
            view = v;
            repoName = view.findViewById(R.id.repo_name);
            authorImage = view.findViewById(R.id.author_image);
            authorName = view.findViewById(R.id.author_name);
            starsNumber = view.findViewById(R.id.stars_number);
            forksNumber = view.findViewById(R.id.forks_number);
            repoDescription = view.findViewById(R.id.repo_description);

        }
    }

    public void addItems(List<Items> items) {
        mRepoList.addAll(items);
        notifyDataSetChanged();
    }

}
