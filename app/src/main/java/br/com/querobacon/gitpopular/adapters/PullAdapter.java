package br.com.querobacon.gitpopular.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.querobacon.gitpopular.R;
import br.com.querobacon.gitpopular.rest.model.Pull;

/**
 * Created by arthu on 02/03/2018.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolder> {

    private List<Pull> mPullList;
    private Context mContext;

    public PullAdapter(Context context, List<Pull> item) {
        mPullList = item;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_pull_list, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Pull pull = mPullList.get(position);
        holder.authorName.setText(pull.getUser().getLogin());
        Picasso.with(mContext).load(pull.getUser().getAvatar_url()).into(holder.authorImage);
        holder.title.setText(pull.getTitle());
        holder.createDate.setText(mContext.getResources().getString(R.string.create_date, pull.getCreated_at()));
        holder.updateDate.setText(mContext.getResources().getString(R.string.update_date, pull.getUpdated_at()));
        holder.body.setText(pull.getBody());

        holder.pullView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getHtml_url()));
                mContext.startActivity(browserIntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mPullList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        LinearLayout pullView;
        TextView authorName;
        ImageView authorImage;
        TextView title;
        TextView createDate;
        TextView updateDate;
        TextView body;

        public ViewHolder(View v) {
            super(v);
            view = v;
            pullView = view.findViewById(R.id.pull_view);
            authorName = view.findViewById(R.id.author_name);
            authorImage = view.findViewById(R.id.author_image);
            title = view.findViewById(R.id.pull_title);
            createDate = view.findViewById(R.id.pull_create_date);
            updateDate = view.findViewById(R.id.pull_update_date);
            body = view.findViewById(R.id.pull_body);
        }
    }

}