package br.com.querobacon.gitpopular;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import br.com.querobacon.gitpopular.adapters.PullAdapter;
import br.com.querobacon.gitpopular.rest.BaseClient;
import br.com.querobacon.gitpopular.rest.model.Pull;
import br.com.querobacon.gitpopular.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestActivity extends AppCompatActivity {

    private RecyclerView mPullList;
    private BaseClient mClient;
    private PullAdapter mPullAdapter;
    private LinearLayoutManager mLayoutManager;
    private String mOwnerName;
    private String mRepoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        mClient = new BaseClient();
        mLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mPullList = (RecyclerView) findViewById(R.id.pull_list);
        mPullList.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL);
        mPullList.addItemDecoration(dividerItemDecoration);

        if (getIntent().getExtras() != null) {
            mOwnerName = getIntent().getStringExtra(Constants.OWNER_NAME);
            mRepoName = getIntent().getStringExtra(Constants.REPO_NAME);

            final Call<List<Pull>> pulls = mClient.getApi().getPulls(mOwnerName, mRepoName);
            pulls.enqueue(new Callback<List<Pull>>() {
                @Override
                public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                    if (response.isSuccessful()) {
                        if (!response.body().isEmpty()) {
                            mPullAdapter = new PullAdapter(PullRequestActivity.this, response.body());
                            mPullList.setAdapter(mPullAdapter);
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Pull>> call, Throwable t) {
                    Toast.makeText(PullRequestActivity.this, getString(R.string.error),Toast.LENGTH_LONG).show();
                }
            });
        }

    }

}
